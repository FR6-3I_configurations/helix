<div align='center'>

# Helix

</div>

## 📦 Dependencies (Arch)

### ❗ Required

Official :

```shell
paru -S helix
```

### ➕ Optional

Official :

```shell
paru -S \
  ansible-language-server \
  bash-language-server \
  eslint-language-server \
  dot-language-server \
  lldb \
  lua-language-server \
  marksman \
  prettier \
  rust-analyzer \
  sd \
  taplo-cli \
  typescript-language-server \
  vscode-css-languageserver \
  vscode-html-languageserver \
  vscode-json-languageserver \
  xclip \
  yaml-language-server
```

AUR :

```shell
paru -S dockerfile-language-server prisma-language-server
```
